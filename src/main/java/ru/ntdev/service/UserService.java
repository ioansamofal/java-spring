
package ru.ntdev.service;

import java.util.List;
import ru.ntdev.dto.UserDto;
import ru.ntdev.entity.User;


public interface UserService {

    List<UserDto> listUsers(List<User> users);
    List<UserDto> test();
    public Iterable<User> users();
    public User findOne(int id);
}
