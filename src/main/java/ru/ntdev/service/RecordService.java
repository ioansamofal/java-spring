package ru.ntdev.service;

import ru.ntdev.entity.Record;

public interface RecordService {

    public Iterable<Record> records();
    public Record findById(int id);
    public Iterable<Record> findAll();
    public Record save(Record record);
}
