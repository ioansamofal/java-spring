package ru.ntdev.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ntdev.entity.Record;
import ru.ntdev.repository.RecordRepository;

@Service
public class RecordServiceImpl implements RecordService{

    @Autowired
    RecordRepository repository;

    @Override
    public Record findById(int id)
    {
        System.out.println("something record one");
        return this.repository.findById(id);
    }

    @Override
    public Iterable<Record> records()
    {
        System.out.println("something record list");
        return this.repository.findAll();
    }

    @Override
    public Iterable<Record> findAll()
    {
        return this.repository.findAll();
    }

    @Override
    public Record save(Record record)
    {
        return this.repository.save(record);
    }
}
