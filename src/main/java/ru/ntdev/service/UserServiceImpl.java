package ru.ntdev.service;

import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ntdev.dto.UserDto;
import ru.ntdev.entity.User;
import ru.ntdev.repository.UserRepository;


import java.util.HashSet;
import java.util.List;
import javax.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ntdev.entity.UserProperty;

/**
 *
 * @author vminakov
 */
@Service
public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    private UserRepository userRepository;



    public List<UserDto> listUsers(List<User> users) {

        List<UserDto> _dtos = new ArrayList<>();

        UserDto _dto;
        for(User _u : users) {
            _dto = new UserDto();
            _dto.setId(_u.getId());
            _dto.setUsername(_u.getUsername());

            logger.info("Пользовател "+_u.getUsername());
            logger.info("Cвойства пользователя "+Integer.toString(_u.getProperties().size()));

            if(_u.getProperties().size() > 0) {
                for(UserProperty _p : _u.getProperties()) {
                    logger.info("Элементы свойства пользователя "+Integer.toString(_p.getItems().size()));
                }
            }

            _dtos.add(_dto);
        }

        return _dtos;
    }

    @Override
    @Transactional
    public List<UserDto> test() {
        List<User> _users =  userRepository.findAll();

        return listUsers(_users);
    }

    public Iterable<User> users()
    {
        return this.userRepository.findAllByOrderByIdAsc();
    }

    public User findOne(int id)
    {
        return this.userRepository.findOne(id);
    }



}
