package ru.ntdev.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.ntdev.entity.Record;

import java.util.List;

public class RecordDaoImpl implements RecordDao {

    private static final Logger logger = LoggerFactory.getLogger(RecordDaoImpl.class);
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sf)
    {
        this.sessionFactory = sf;
    }

    public List<Record> records()
    {
        Session session = this.sessionFactory.getCurrentSession();
        List<Record> records = session.createQuery("from RecordEntity").list();
        return records;
    }

    public Record findById(int id)
    {
        Session session = this.sessionFactory.getCurrentSession();
        Record record = session.load(Record.class, new Integer(id));
        return record;
    }
}
