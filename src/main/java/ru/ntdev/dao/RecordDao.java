package ru.ntdev.dao;

import ru.ntdev.entity.Record;

import java.util.List;

public interface RecordDao {
    public List<Record> records();

    public Record findById(int id);
}
