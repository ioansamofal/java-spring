package ru.ntdev.response;

import java.util.List;
import ru.ntdev.dto.UserDto;

/**
 *
 * @author vminakov
 */
public class JsonResponse {

    public final static String STATUS_OK = "OK";

    private String status;
    private String message;
    private List<UserDto> users;

    /**
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the users
     */
    public List<UserDto> getUsers() {
        return users;
    }

    /**
     * @param users the users to set
     */
    public void setUsers(List<UserDto> users) {
        this.users = users;
    }

}
