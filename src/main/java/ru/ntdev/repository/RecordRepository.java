package ru.ntdev.repository;

import org.springframework.data.repository.CrudRepository;
import ru.ntdev.entity.Record;

import java.util.List;

public interface RecordRepository extends CrudRepository<Record, Integer> {
    Record findById(int id);
    List<Record> findAll();

    public Record save(Record record);
}
