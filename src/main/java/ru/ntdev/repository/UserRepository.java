package ru.ntdev.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import ru.ntdev.entity.User;

public interface UserRepository extends CrudRepository<User, Integer> {
    User findByUsername(String username);
    List<User> findAll();
    List<User> findAllByOrderByIdAsc();
}
