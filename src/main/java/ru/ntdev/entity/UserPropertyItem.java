package ru.ntdev.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author vminakov
 */
@Entity
@Table(name = "property_items")
public class UserPropertyItem {

    @Id
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "property_id",referencedColumnName = "id")
    private UserProperty property;

    private String title;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the property
     */
    public UserProperty getProperty() {
        return property;
    }

    /**
     * @param property the property to set
     */
    public void setProperty(UserProperty property) {
        this.property = property;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }



}
