package ru.ntdev.entity;

import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author vminakov
 */
@Entity
@Table(name = "user_properties")
public class UserProperty implements Serializable {

    @Id
    @SequenceGenerator(name="user_property_id_seq",sequenceName="user_property_id_seq",allocationSize=1)
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator="user_property_id_seq")
    private Integer id;

    @Column(name = "property_name")
    private String name;

    @Column(name = "user_id")
    private Integer userId;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "property_id",referencedColumnName = "id")
    private Set<UserPropertyItem> items;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the items
     */
    public Set<UserPropertyItem> getItems() {
        return items;
    }

    /**
     * @param items the items to set
     */
    public void setItems(Set<UserPropertyItem> items) {
        this.items = items;
    }

}
