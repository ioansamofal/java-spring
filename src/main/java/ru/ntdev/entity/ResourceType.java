package ru.ntdev.entity;

import javax.persistence.*;

@Entity
@Table(name = "resource_types")
public class ResourceType {

    @Id
    @SequenceGenerator(name = "resources_type_id_seq", sequenceName = "resources_type_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "resources_type_id_seq")
    private int id;
    private String code;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
