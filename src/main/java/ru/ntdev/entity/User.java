/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.ntdev.entity;

import java.util.Date;
import java.util.Set;
import javax.persistence.*;

/**
 *
 * @author vminakov
 */
@Entity
@Table(name = "users")
public class User {

    @Id
    @SequenceGenerator(name="users_id_seq",
                       sequenceName="users_id_seq",
                       allocationSize=1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
                    generator="users_id_seq")
    @Column(name = "id", updatable=false)
    private int id;

    @Column(unique = true)
    private String username;
    private String password;
    private String about;
    private String avatar;
    private Date created_at;
    private String date_birth;
    private String city;

    @OneToMany(targetEntity = UserProperty.class,fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private Set<UserProperty> properties;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "user_roles", joinColumns = {
                @JoinColumn(name = "user_id", nullable = false, updatable = false) },
                inverseJoinColumns = { @JoinColumn(name = "role_id", nullable = false, updatable = false)
    })
    private Set<Role> roles;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the properties
     */
    public Set<UserProperty> getProperties() {
        return properties;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    /**
     * @param properties the properties to set
     */
    public void setProperties(Set<UserProperty> properties) {
        this.properties = properties;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Date getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Date created_at) {
        this.created_at = created_at;
    }

    public String getDate_birth() {
        return date_birth;
    }

    public void setDate_birth(String date_birth) {
        this.date_birth = date_birth;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

}
