package ru.ntdev.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "roles")
public class Role {
    @Id
    @SequenceGenerator(name = "roles_id_seq",
                        sequenceName = "roles_id_seq",
                        allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
                    generator = "roles_id_seq")
    @Column(name = "id")
    private int id;
    private String code;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "role_resources", joinColumns = {
            @JoinColumn(name = "role_id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "resource_id", nullable = false, updatable = false)
    })
    private Set<Resource> resources;



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<Resource> getResources() {
        return resources;
    }

    public void setResources(Set<Resource> resources) {
        this.resources = resources;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


}
