package ru.ntdev.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.ntdev.entity.Record;
import ru.ntdev.entity.User;
import ru.ntdev.service.RecordService;
import ru.ntdev.service.UserService;

import java.sql.Timestamp;
import java.util.Date;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

@Controller
public class RecordController {

    @Autowired
    private RecordService recordService;
    @Autowired
    private UserService userService;

    private static final Logger logger = LoggerFactory.getLogger(RecordController.class);

    @RequestMapping(value = "/records")
    public String records(Model model)
    {
        System.out.println("action records list:");
        model.addAttribute("allRecords", this.recordService.records());
        model.addAttribute("title", "Records List");
        return "records";
    }

    @RequestMapping(value = "/record-page")
    public String recordPage(@RequestParam(value = "id", required = true) short id, Model model)
    {
        Record record = this.recordService.findById(id);
        model.addAttribute("record", record);
        return "recordpage";
    }

    @RequestMapping(value = "/record-edit")
    public String recordEdit(@RequestParam(value = "id", required = true) short id, Model model)
    {
        Record record = this.recordService.findById(id);
        model.addAttribute("record", record);
        model.addAttribute("userList", this.userService.users());
        model.addAttribute("action", "edit");
        return "saverecord";
    }

    @RequestMapping(value = "/recordslist")
    public String recordslist(Model model)
    {
        model.addAttribute("recordslist", this.recordService.findAll());
        return "recordslist";
    }

    @RequestMapping(value = "/save-record")
    public String saveRecord(Model model)
    {
        Record record = new Record();
        record.setText("this is new nano text");
        record.setTitle("very interesting title");
        record.setUser_id(1);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        record.setCreated_at(timestamp);
        this.recordService.save(record);
        model.addAttribute("record", record);
        model.addAttribute("title", record.getTitle());
        return "newrecord";
    }

    @GetMapping(value = "/saverecord")
    public String recordLoad(Model model)
    {
        model.addAttribute("userList", this.userService.users());
        model.addAttribute("record", new Record());
        model.addAttribute("action", "create");
        return "saverecord";
    }

    @PostMapping("/saverecord")
    public String recordSubmit(@ModelAttribute("record") Record record, Model model)
    {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        record.setCreated_at(timestamp);
        this.recordService.save(record);
        model.addAttribute("userList", this.userService.users());
//        model.addAttribute("record", record);

        return "saverecord";
    }
}
