package ru.ntdev.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.ntdev.response.JsonResponse;
import ru.ntdev.service.UserService;


/**
 *
 * @author vminakov
 */
@RestController
public class
MainController {

    @Autowired
    UserService userService;

    @GetMapping(value = "/test")
    public @ResponseBody JsonResponse test() {
        JsonResponse response = new JsonResponse();

        response.setUsers(userService.test());

        response.setStatus(JsonResponse.STATUS_OK);
        return response;
    }


}
