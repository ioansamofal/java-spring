package ru.ntdev.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.ntdev.entity.User;
import ru.ntdev.service.UserService;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService userService;

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String listPersons(Model model)
    {
        model.addAttribute("userList", this.userService.users());
        model.addAttribute("title", "Users List");
        return "users";
    }

    @GetMapping(value = "/user")
    public String userPage(@RequestParam(value = "id", required = true) int id, Model model)
    {
        model.addAttribute("user", this.userService.findOne(id));
        return "userpage";
    }

    @GetMapping(value = "/about")
    public String about(Model model)
    {
        model.addAttribute("title", "About Page");
        return "about";
    }
}
