<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
<head>

	<style type="text/css">
		.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
		.tg .tg-4eph{background-color:#f9f9f9}
	</style>
        <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
        <script type="text/javascript">

	$(document).ready(function() {

		$('#userDTO').submit(function(e) {
			var frm = $('#userDTO');
			e.preventDefault();

		    var data = {}
		    var Form = this;

		    //Gather Data also remove undefined keys(buttons)
		    $.each(this, function(i, v){
		            var input = $(v);
		        data[input.attr("name")] = input.val();
		        delete data["undefined"];
		    });
        $.ajax({
            contentType : 'application/json; charset=utf-8',
            type: frm.attr('method'),
            url: frm.attr('action'),
            dataType : 'json',
            data : JSON.stringify(data),
            success : function(callback){
            	alert("Response: Status: "+callback.status);
                $(this).html("Success!");
            },
            error : function(){
                $(this).html("Error!");
            }
        });
		});
	});
</script>
</head>
<body>
<br>

<c:if test="${!empty result}">
    <h3>${result}</h3>
</c:if>

<h3>User List</h3>
	<table class="tg">
	<tr>
		<th width="80">User ID</th>
		<th width="120">User Name</th>
		<th width="120">User Password</th>
	</tr>
	<c:forEach items="${users}" var="user">
		<tr>
			<td>${user.id}</td>
			<td>${user.username}</td>
			<td>${user.password}</td>
		</tr>
	</c:forEach>
	</table>
<c:if test="${!empty new_user}">
    <spring:url value="/addajax" var="userActionUrl" />
    <form:form id="userDTO" method="post"  modelAttribute="new_user" action="${userActionUrl}">

		<form:hidden path="id" />
		<spring:bind path="username">
			<label>Логин</label>
			<form:input path="username" type="text" id="username" placeholder="Логин" />
                        <form:errors path="username" class="control-label" />
		</spring:bind>
                        
                <form:select path="type">
                   <form:option value="NONE" label="--- Select ---"/>
                   <form:options items="${usertypes}" itemLabel="type" itemValue="type"/>
                </form:select>

		<spring:bind path="password">
			<label>password</label>
                        <form:input path="password" id="email" placeholder="Пароль" />
			<form:errors path="password"/>
		</spring:bind>
                <button type="submit">Добавить</button>
    </form:form>
</c:if>
</body>
</html>
