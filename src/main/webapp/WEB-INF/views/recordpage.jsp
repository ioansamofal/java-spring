<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page session="false" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="layouts/header.jsp" />
<jsp:include page="layouts/menu.jsp" />

<h1>${record.title}</h1>
<div>${record.text}</div>
<div><span>
    <fmt:parseDate value="${record.created_at}" pattern="yyyy-MM-dd HH:mm" var="createdAt"/>
    <fmt:formatDate value="${createdAt}" pattern="dd-MM-yyyy HH:mm" />
</span>
    <span>Author: ${record.user.username}</span>
</div>

${record.comments}
<section>
    <h3>Comments:</h3>
    <ul>
        <c:forEach items="${record.comments}" var="comment">
            <li>
                <p>${comment.text}</p>
                <p>Posted at:
                    <fmt:parseDate value="${comment.created_at}" pattern="yyyy-MM-dd HH:mm" var="createdAt"/>
                    <fmt:formatDate value="${createdAt}" pattern="dd-MM-yyyy HH:mm" />
                </p>
                <p>Author: ${comment.user.username}</p>
            </li>
        </c:forEach>
    </ul>
</section>

<jsp:include page="layouts/footer.jsp" />