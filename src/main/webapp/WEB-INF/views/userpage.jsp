<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page session="false" %>

<html>
<head>
    <title>User Page</title>
</head>
<body>
<jsp:include page="layouts/menu.jsp" />
<br>
<h3>User Profile</h3>
<img src="/images/avatars/${user.id}.png" alt=""/>
<h2><a href="/simpleweb/user?id=${user.id}">${user.username}</a></h2>
<span>Created at:
    <fmt:parseDate value="${user.created_at}" pattern="yyyy-MM-dd HH:mm" var="createdAt"/>
    <fmt:formatDate value="${createdAt}" pattern="dd-MM-yyyy HH:mm" />
</span>
</body>
</html>
