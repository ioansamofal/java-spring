<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page session="false" %>

<html>
<head>
    <title>List records</title>
</head>
<body>
<jsp:include page="layouts/menu.jsp" />

<c:if test="${empty recordslist}">
    Записи почему-то пустыые
</c:if>
<c:if test="${!empty recordslist}">
    <h1>Не пустой</h1>
</c:if>

<table>
    <tr>
        <td>ID</td>
        <td>Title</td>
        <td>Text</td>
        <td>Created at</td>
        <td>Author Name</td>
        <td>Comments</td>
        <td>Edit</td>
    </tr>
    <c:forEach items="${recordslist}" var="record">
        <tr>
            <td>${record.id}</td>
            <td><a href="/simpleweb/record-page?id=${record.id}">${record.title}</a></td>
            <td>${record.text}</td>
            <td>
                <fmt:parseDate value="${record.created_at}" pattern="yyyy-MM-dd HH:mm" var="createdAt"/>
                <fmt:formatDate value="${createdAt}" pattern="dd-MM-yyyy HH:mm" />
            </td>
            <td>${record.user.username} </td>
            <td>
                <c:forEach items="${record.comments}" var="comment">
                    <p>
                        <span>${comment.text}</span>
                        <span>
                            <fmt:parseDate value="${comment.created_at}" pattern="yyyy-MM-dd HH:mm" var="createdAt"/>
                            <fmt:formatDate value="${createdAt}" pattern="dd-MM-yyyy HH:mm" />
                        </span>
                        <span>${comment.record.id}</span>
                    </p>
                </c:forEach>
            </td>
            <td><a href="/simpleweb/record-edit?id=${record.id}">Edit</a></td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
