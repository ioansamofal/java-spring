<jsp:include page="layouts/header.jsp" />
<jsp:include page="layouts/menu.jsp" />

<c:if test="${empty allRecords}">
 Записи почему-то пустыые
</c:if>
<table>
    <tr>
        <td>Title</td>
        <td>Text</td>
        <td>Author</td>
    </tr>
    <c:forEach items="${allRecords}" var="record">
        <tr>
            <td>${record.title}</td>
            <td>${record.text}</td>
            <td>${record.author_id}</td>
        </tr>
    </c:forEach>
</table>

<jsp:include page="layouts/footer.jsp" />