<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page session="false" %>
<html xmlns:th="http://www.thymeleaf.org">
<head>
    <title>${record.getTitle()}</title>
</head>
<body>


<form action="/simpleweb/saverecord" th:action="@{/saverecord}" th:object="${record}" method="post">
    <input type="text" name="title" th:field="*{title}" value ="${record.title}" placeholder="enter title" />
    <textarea th:field="*{text}" name="text" placeholder="enter text">${record.text}</textarea>
    <input type="hidden" name="id" value="${record.id}"/>
    <select name="author_id">
        <c:forEach items="${userList}" var="user">
            <c:choose>
                <c:when test="${user.id == record.author_id}">
                    <option value="${user.id}" selected="selected">${user.username}</option>
                </c:when>
                <c:otherwise>
                    <option value="${user.id}">${user.username}</option>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </select>
    <select name="user_id">
        <c:forEach items="${userList}" var="user">
            <c:choose>
                <c:when test="${user.id == record.user_id}">
                    <option value="${user.id}" selected="selected">${user.username}</option>
                </c:when>
                <c:otherwise>
                    <option value="${user.id}">${user.username}</option>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </select>
    <input type="submit" value="Submit!" />
</form>

<c:if test="${action == 'create'}">
    <div>
        <p>Title: ${record.title}</p>
        <p>Text: ${record.text}</p>
        <p>Text: ${record.author_id}</p>
        <p>Text: ${record.user_id}</p>
    </div>
</c:if>

</body>
</html>
