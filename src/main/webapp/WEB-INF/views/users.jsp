<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page session="false" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<jsp:include page="layouts/header.jsp" />
<jsp:include page="layouts/menu.jsp" />
<br>
<h3>Users List</h3>
<c:if test="${!empty userList}">
    <table class="tg">
        <tr>
            <th width="80">User ID</th>
            <th width="120">User Name</th>
            <th width="120">User Password</th>
            <th width="120">User Roles</th>
            <th width="120">Created At:</th>
            <th width="120">City:</th>
        </tr>
        <c:forEach items="${userList}" var="user">
            <tr>
                <td>${user.id}</td>
                <td>${user.username}</td>
                <td>${user.password}</td>
                <td>
                    <c:forEach items="${user.roles}" var="role">
                        <p>${role.code}
                        [
                        <c:forEach items="${role.resources}" var="res">
                            <span>${res.type.code} : ${res.code}</span><br/>
                        </c:forEach>
                            ]</p>
                    </c:forEach>
                </td>
                <td>
                    <fmt:parseDate value="${user.date_birth}" pattern="yyyy-MM-dd HH:mm" var="dateBirth"/>
                    <fmt:formatDate value="${dateBirth}" pattern="dd.MM.yy HH:mm" />
                </td>
                <td>${user.city}</td>
            </tr>
        </c:forEach>
    </table>
</c:if>
<c:if test="${empty userList}">
    <h1>dfdfdf</h1>
</c:if>


<jsp:include page="layouts/footer.jsp" />