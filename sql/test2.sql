CREATE TABLE records (
  id INT DEFAULT 0 PRIMARY KEY,
  title VARCHAR (60) NOT NULL,
  text TEXT NOT NULL,
  created_at TIMESTAMP ,
  author_id INT DEFAULT 0 NOT NULL
)

CREATE TABLE comments (
  id INT PRIMARY KEY,
  text VARCHAR(1000),
  created_at TIMESTAMP,
  user_id INT,
  record_id INT
)

CREATE TABLE record_comments (
  id INT PRIMARY KEY,
  record_id INT,
  comment_id INT
)

CREATE SEQUENCE comments_id_seq
  START WITH 1;

alter table comments alter id set default nextval('comments_id_seq') + 3;


CREATE SEQUENCE record_comments_id_seq
  START WITH 1;
alter table record_comments alter id set default nextval('record_comments_id_seq') + 5;

ALTER TABLE users
    ADD COLUMN about VARCHAR(1000);

ALTER TABLE users
  ADD COLUMN avatar VARCHAR(255);

ALTER TABLE users
  ADD COLUMN created_at TIMESTAMP;

ALTER TABLE users
  ADD COLUMN date_birth TIMESTAMP;

ALTER TABLE users
  ADD COLUMN city VARCHAR(50);