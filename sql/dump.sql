PGDMP         &            
    v            test_1    9.5.14    9.5.14 \    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �           1262    42334    test_1    DATABASE     x   CREATE DATABASE test_1 WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'ru_RU.UTF-8' LC_CTYPE = 'ru_RU.UTF-8';
    DROP DATABASE test_1;
             test    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            �           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    7            �           0    0    SCHEMA public    ACL     �   REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;
                  postgres    false    7                        3079    12397    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            �           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    42406    groups    TABLE     U   CREATE TABLE public.groups (
    id integer NOT NULL,
    title character varying
);
    DROP TABLE public.groups;
       public         test    false    7            �            1259    42412    groups_id_seq    SEQUENCE     v   CREATE SEQUENCE public.groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.groups_id_seq;
       public       test    false    7    195            �           0    0    groups_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.groups_id_seq OWNED BY public.groups.id;
            public       test    false    196            �            1259    42414    property_items    TABLE     v   CREATE TABLE public.property_items (
    id integer NOT NULL,
    property_id integer,
    title character varying
);
 "   DROP TABLE public.property_items;
       public         test    false    7            �            1259    42420    property_items_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.property_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.property_items_id_seq;
       public       test    false    7    197            �           0    0    property_items_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.property_items_id_seq OWNED BY public.property_items.id;
            public       test    false    198            �            1259    50527    records    TABLE       CREATE TABLE public.records (
    id integer DEFAULT 0 NOT NULL,
    title character varying(60) NOT NULL,
    text text NOT NULL,
    created_at timestamp without time zone,
    author_id integer DEFAULT 0 NOT NULL,
    user_id integer DEFAULT 0 NOT NULL
);
    DROP TABLE public.records;
       public         test    false    7            �            1259    50537    records_id_seq    SEQUENCE     w   CREATE SEQUENCE public.records_id_seq
    START WITH 5
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 5;
 %   DROP SEQUENCE public.records_id_seq;
       public       test    false    7            �            1259    42335    resource_types    TABLE     \   CREATE TABLE public.resource_types (
    id integer NOT NULL,
    code character varying
);
 "   DROP TABLE public.resource_types;
       public         test    false    7            �            1259    42341    resource_types_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.resource_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.resource_types_id_seq;
       public       test    false    181    7            �           0    0    resource_types_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.resource_types_id_seq OWNED BY public.resource_types.id;
            public       test    false    182            �            1259    42343 	   resources    TABLE     l   CREATE TABLE public.resources (
    id integer NOT NULL,
    code character varying,
    type_id integer
);
    DROP TABLE public.resources;
       public         test    false    7            �            1259    42349    resources_id_seq    SEQUENCE     y   CREATE SEQUENCE public.resources_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.resources_id_seq;
       public       test    false    183    7            �           0    0    resources_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.resources_id_seq OWNED BY public.resources.id;
            public       test    false    184            �            1259    42351    role_resources    TABLE     n   CREATE TABLE public.role_resources (
    id integer NOT NULL,
    role_id integer,
    resource_id integer
);
 "   DROP TABLE public.role_resources;
       public         test    false    7            �            1259    42354    role_resources_id_seq    SEQUENCE     ~   CREATE SEQUENCE public.role_resources_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.role_resources_id_seq;
       public       test    false    185    7            �           0    0    role_resources_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.role_resources_id_seq OWNED BY public.role_resources.id;
            public       test    false    186            �            1259    42356    roles    TABLE     S   CREATE TABLE public.roles (
    id integer NOT NULL,
    code character varying
);
    DROP TABLE public.roles;
       public         test    false    7            �            1259    42362    roles_id_seq    SEQUENCE     u   CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.roles_id_seq;
       public       test    false    7    187            �           0    0    roles_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;
            public       test    false    188            �            1259    42422    user_groups    TABLE     h   CREATE TABLE public.user_groups (
    id integer NOT NULL,
    user_id integer,
    group_id integer
);
    DROP TABLE public.user_groups;
       public         test    false    7            �            1259    42425    user_groups_id_seq    SEQUENCE     {   CREATE SEQUENCE public.user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.user_groups_id_seq;
       public       test    false    199    7            �           0    0    user_groups_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.user_groups_id_seq OWNED BY public.user_groups.id;
            public       test    false    200            �            1259    42427    user_properties    TABLE     {   CREATE TABLE public.user_properties (
    id integer NOT NULL,
    user_id integer,
    property_name character varying
);
 #   DROP TABLE public.user_properties;
       public         test    false    7            �            1259    42433    user_properties_id_seq    SEQUENCE        CREATE SEQUENCE public.user_properties_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.user_properties_id_seq;
       public       test    false    7    201            �           0    0    user_properties_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.user_properties_id_seq OWNED BY public.user_properties.id;
            public       test    false    202            �            1259    42364 
   user_roles    TABLE     f   CREATE TABLE public.user_roles (
    id integer NOT NULL,
    user_id integer,
    role_id integer
);
    DROP TABLE public.user_roles;
       public         test    false    7            �            1259    42367    user_roles_id_seq    SEQUENCE     z   CREATE SEQUENCE public.user_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.user_roles_id_seq;
       public       test    false    7    189            �           0    0    user_roles_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.user_roles_id_seq OWNED BY public.user_roles.id;
            public       test    false    190            �            1259    42369 
   user_types    TABLE     �   CREATE TABLE public.user_types (
    id integer NOT NULL,
    type_code character varying,
    description character varying
);
    DROP TABLE public.user_types;
       public         test    false    7            �            1259    42375    user_types_id_seq    SEQUENCE     z   CREATE SEQUENCE public.user_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.user_types_id_seq;
       public       test    false    191    7            �           0    0    user_types_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.user_types_id_seq OWNED BY public.user_types.id;
            public       test    false    192            �            1259    42377    users    TABLE     �   CREATE TABLE public.users (
    id integer NOT NULL,
    username character varying,
    password character varying,
    type_code character varying
);
    DROP TABLE public.users;
       public         test    false    7            �            1259    42383    users_id_seq    SEQUENCE     u   CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       test    false    7    193            �           0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
            public       test    false    194            7           2604    42435    id    DEFAULT     f   ALTER TABLE ONLY public.groups ALTER COLUMN id SET DEFAULT nextval('public.groups_id_seq'::regclass);
 8   ALTER TABLE public.groups ALTER COLUMN id DROP DEFAULT;
       public       test    false    196    195            8           2604    42436    id    DEFAULT     v   ALTER TABLE ONLY public.property_items ALTER COLUMN id SET DEFAULT nextval('public.property_items_id_seq'::regclass);
 @   ALTER TABLE public.property_items ALTER COLUMN id DROP DEFAULT;
       public       test    false    198    197            0           2604    42385    id    DEFAULT     v   ALTER TABLE ONLY public.resource_types ALTER COLUMN id SET DEFAULT nextval('public.resource_types_id_seq'::regclass);
 @   ALTER TABLE public.resource_types ALTER COLUMN id DROP DEFAULT;
       public       test    false    182    181            1           2604    42386    id    DEFAULT     l   ALTER TABLE ONLY public.resources ALTER COLUMN id SET DEFAULT nextval('public.resources_id_seq'::regclass);
 ;   ALTER TABLE public.resources ALTER COLUMN id DROP DEFAULT;
       public       test    false    184    183            2           2604    42387    id    DEFAULT     v   ALTER TABLE ONLY public.role_resources ALTER COLUMN id SET DEFAULT nextval('public.role_resources_id_seq'::regclass);
 @   ALTER TABLE public.role_resources ALTER COLUMN id DROP DEFAULT;
       public       test    false    186    185            3           2604    42388    id    DEFAULT     d   ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);
 7   ALTER TABLE public.roles ALTER COLUMN id DROP DEFAULT;
       public       test    false    188    187            9           2604    42437    id    DEFAULT     p   ALTER TABLE ONLY public.user_groups ALTER COLUMN id SET DEFAULT nextval('public.user_groups_id_seq'::regclass);
 =   ALTER TABLE public.user_groups ALTER COLUMN id DROP DEFAULT;
       public       test    false    200    199            :           2604    42438    id    DEFAULT     x   ALTER TABLE ONLY public.user_properties ALTER COLUMN id SET DEFAULT nextval('public.user_properties_id_seq'::regclass);
 A   ALTER TABLE public.user_properties ALTER COLUMN id DROP DEFAULT;
       public       test    false    202    201            4           2604    42389    id    DEFAULT     n   ALTER TABLE ONLY public.user_roles ALTER COLUMN id SET DEFAULT nextval('public.user_roles_id_seq'::regclass);
 <   ALTER TABLE public.user_roles ALTER COLUMN id DROP DEFAULT;
       public       test    false    190    189            5           2604    42390    id    DEFAULT     n   ALTER TABLE ONLY public.user_types ALTER COLUMN id SET DEFAULT nextval('public.user_types_id_seq'::regclass);
 <   ALTER TABLE public.user_types ALTER COLUMN id DROP DEFAULT;
       public       test    false    192    191            6           2604    42439    id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public       test    false    194    193            �          0    42406    groups 
   TABLE DATA               +   COPY public.groups (id, title) FROM stdin;
    public       test    false    195   M[       �           0    0    groups_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.groups_id_seq', 1, false);
            public       test    false    196            �          0    42414    property_items 
   TABLE DATA               @   COPY public.property_items (id, property_id, title) FROM stdin;
    public       test    false    197   j[       �           0    0    property_items_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.property_items_id_seq', 3, true);
            public       test    false    198            �          0    50527    records 
   TABLE DATA               R   COPY public.records (id, title, text, created_at, author_id, user_id) FROM stdin;
    public       test    false    203   �[       �           0    0    records_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.records_id_seq', 69, true);
            public       test    false    204            �          0    42335    resource_types 
   TABLE DATA               2   COPY public.resource_types (id, code) FROM stdin;
    public       test    false    181   �\       �           0    0    resource_types_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.resource_types_id_seq', 2, true);
            public       test    false    182            �          0    42343 	   resources 
   TABLE DATA               6   COPY public.resources (id, code, type_id) FROM stdin;
    public       test    false    183    ]       �           0    0    resources_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.resources_id_seq', 4, true);
            public       test    false    184            �          0    42351    role_resources 
   TABLE DATA               B   COPY public.role_resources (id, role_id, resource_id) FROM stdin;
    public       test    false    185   8]       �           0    0    role_resources_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.role_resources_id_seq', 5, true);
            public       test    false    186            �          0    42356    roles 
   TABLE DATA               )   COPY public.roles (id, code) FROM stdin;
    public       test    false    187   o]       �           0    0    roles_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.roles_id_seq', 2, true);
            public       test    false    188            �          0    42422    user_groups 
   TABLE DATA               <   COPY public.user_groups (id, user_id, group_id) FROM stdin;
    public       test    false    199   �]       �           0    0    user_groups_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.user_groups_id_seq', 1, false);
            public       test    false    200            �          0    42427    user_properties 
   TABLE DATA               E   COPY public.user_properties (id, user_id, property_name) FROM stdin;
    public       test    false    201   �]       �           0    0    user_properties_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.user_properties_id_seq', 3, true);
            public       test    false    202            �          0    42364 
   user_roles 
   TABLE DATA               :   COPY public.user_roles (id, user_id, role_id) FROM stdin;
    public       test    false    189   �]       �           0    0    user_roles_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.user_roles_id_seq', 3, true);
            public       test    false    190            �          0    42369 
   user_types 
   TABLE DATA               @   COPY public.user_types (id, type_code, description) FROM stdin;
    public       test    false    191   ^       �           0    0    user_types_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.user_types_id_seq', 2, true);
            public       test    false    192            �          0    42377    users 
   TABLE DATA               B   COPY public.users (id, username, password, type_code) FROM stdin;
    public       test    false    193   ;^        	           0    0    users_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.users_id_seq', 5, true);
            public       test    false    194            M           2606    42441    groups_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.groups DROP CONSTRAINT groups_pkey;
       public         test    false    195    195            O           2606    42443    property_items_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.property_items
    ADD CONSTRAINT property_items_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.property_items DROP CONSTRAINT property_items_pkey;
       public         test    false    197    197            U           2606    50536    records_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public.records
    ADD CONSTRAINT records_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.records DROP CONSTRAINT records_pkey;
       public         test    false    203    203            ?           2606    42393    resource_types_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.resource_types
    ADD CONSTRAINT resource_types_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.resource_types DROP CONSTRAINT resource_types_pkey;
       public         test    false    181    181            A           2606    42395    resources_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.resources
    ADD CONSTRAINT resources_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY public.resources DROP CONSTRAINT resources_pkey;
       public         test    false    183    183            C           2606    42397    role_resources_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.role_resources
    ADD CONSTRAINT role_resources_pkey PRIMARY KEY (id);
 L   ALTER TABLE ONLY public.role_resources DROP CONSTRAINT role_resources_pkey;
       public         test    false    185    185            E           2606    42399 
   roles_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.roles DROP CONSTRAINT roles_pkey;
       public         test    false    187    187            Q           2606    42445    user_groups_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.user_groups
    ADD CONSTRAINT user_groups_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY public.user_groups DROP CONSTRAINT user_groups_pkey;
       public         test    false    199    199            S           2606    42447    user_properties_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public.user_properties
    ADD CONSTRAINT user_properties_pkey PRIMARY KEY (id);
 N   ALTER TABLE ONLY public.user_properties DROP CONSTRAINT user_properties_pkey;
       public         test    false    201    201            G           2606    42401    user_roles_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.user_roles
    ADD CONSTRAINT user_roles_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.user_roles DROP CONSTRAINT user_roles_pkey;
       public         test    false    189    189            I           2606    42403    user_types_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public.user_types
    ADD CONSTRAINT user_types_pkey PRIMARY KEY (id);
 D   ALTER TABLE ONLY public.user_types DROP CONSTRAINT user_types_pkey;
       public         test    false    191    191            K           2606    42405 
   users_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.users DROP CONSTRAINT users_pkey;
       public         test    false    193    193            V           2606    42448    user_properties_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.user_properties
    ADD CONSTRAINT user_properties_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);
 V   ALTER TABLE ONLY public.user_properties DROP CONSTRAINT user_properties_user_id_fkey;
       public       test    false    2123    201    193            �      x������ � �      �      x�3�4�,�4�2�F\�`ژ+F��� RdG      �   /  x����r�0�g�)����8�w��.e����\��(M�����1����'�"�p=6��v�w�#�z��ɣ��Oh͈��?�@
��DkRH��CPZ�%H����	����;�86��]�uNn�f��B[�ƛ3���疇�[gC6B�x�߱2:�����B�R�yZ�5d��Mt����t߼��s�Z=<@iQ�u��R�c�t7���>Nm�����$��5�Zi�� "�x�UiI����*�g)��z�U�$%�}��p��?-�w�b]��	���E�WȠ�+�",Df�I�f�i�$��c      �      x�3�,�,H5�2�F\1z\\\ ?5      �   (   x�3�,J-6�4�21�8���Ac��	�a����� ���      �   '   x�3�4�4�2�F\ƜF��\&@҄�(b����� KV(      �      x�3�,��I5�2�F\1z\\\ =y�      �      x������ � �      �      x�3�4�,0�2QF\� ʘ+F��� 8�      �      x�3�4�4�2�F\ƜF@2F��� !��      �      x�3��M�����2�LLɅ0c���� \�F      �   U   x�����  �s;�)-H��K�ID��5,�����.� �f��y��F��qf��4�����5����"pi?wH[-+."�K�g     