--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.14
-- Dumped by pg_dump version 9.5.14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner:
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: groups; Type: TABLE; Schema: public; Owner: test1
--

CREATE TABLE public.groups (
    id integer NOT NULL,
    title character varying
);


ALTER TABLE public.groups OWNER TO test1;

--
-- Name: groups_id_seq; Type: SEQUENCE; Schema: public; Owner: test1
--

CREATE SEQUENCE public.groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.groups_id_seq OWNER TO test1;

--
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test1
--

ALTER SEQUENCE public.groups_id_seq OWNED BY public.groups.id;


--
-- Name: property_items; Type: TABLE; Schema: public; Owner: test1
--

CREATE TABLE public.property_items (
    id integer NOT NULL,
    property_id integer,
    title character varying
);


ALTER TABLE public.property_items OWNER TO test1;

--
-- Name: property_items_id_seq; Type: SEQUENCE; Schema: public; Owner: test1
--

CREATE SEQUENCE public.property_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.property_items_id_seq OWNER TO test1;

--
-- Name: property_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test1
--

ALTER SEQUENCE public.property_items_id_seq OWNED BY public.property_items.id;


--
-- Name: user_groups; Type: TABLE; Schema: public; Owner: test1
--

CREATE TABLE public.user_groups (
    id integer NOT NULL,
    user_id integer,
    group_id integer
);


ALTER TABLE public.user_groups OWNER TO test1;

--
-- Name: user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: test1
--

CREATE SEQUENCE public.user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_groups_id_seq OWNER TO test1;

--
-- Name: user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test1
--

ALTER SEQUENCE public.user_groups_id_seq OWNED BY public.user_groups.id;


--
-- Name: user_properties; Type: TABLE; Schema: public; Owner: test1
--

CREATE TABLE public.user_properties (
    id integer NOT NULL,
    user_id integer,
    property_name character varying
);


ALTER TABLE public.user_properties OWNER TO test1;

--
-- Name: user_properties_id_seq; Type: SEQUENCE; Schema: public; Owner: test1
--

CREATE SEQUENCE public.user_properties_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_properties_id_seq OWNER TO test1;

--
-- Name: user_properties_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test1
--

ALTER SEQUENCE public.user_properties_id_seq OWNED BY public.user_properties.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: test1
--

CREATE TABLE public.users (
    id integer NOT NULL,
    username character varying
);


ALTER TABLE public.users OWNER TO test1;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: test1
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO test1;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: test1
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: test1
--

ALTER TABLE ONLY public.groups ALTER COLUMN id SET DEFAULT nextval('public.groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: test1
--

ALTER TABLE ONLY public.property_items ALTER COLUMN id SET DEFAULT nextval('public.property_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: test1
--

ALTER TABLE ONLY public.user_groups ALTER COLUMN id SET DEFAULT nextval('public.user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: test1
--

ALTER TABLE ONLY public.user_properties ALTER COLUMN id SET DEFAULT nextval('public.user_properties_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: test1
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: groups; Type: TABLE DATA; Schema: public; Owner: test1
--

COPY public.groups (id, title) FROM stdin;
\.


--
-- Name: groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test1
--

SELECT pg_catalog.setval('public.groups_id_seq', 1, false);


--
-- Data for Name: property_items; Type: TABLE DATA; Schema: public; Owner: test1
--

COPY public.property_items (id, property_id, title) FROM stdin;
1	1	pi1
2	1	pi2
3	1	pi3
\.


--
-- Name: property_items_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test1
--

SELECT pg_catalog.setval('public.property_items_id_seq', 3, true);


--
-- Data for Name: user_groups; Type: TABLE DATA; Schema: public; Owner: test1
--

COPY public.user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test1
--

SELECT pg_catalog.setval('public.user_groups_id_seq', 1, false);


--
-- Data for Name: user_properties; Type: TABLE DATA; Schema: public; Owner: test1
--

COPY public.user_properties (id, user_id, property_name) FROM stdin;
1	1	p1
2	1	p2
3	1	p3
\.


--
-- Name: user_properties_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test1
--

SELECT pg_catalog.setval('public.user_properties_id_seq', 3, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: test1
--

COPY public.users (id, username) FROM stdin;
1	user1
2	user2
3	user3
4	user4
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: test1
--

SELECT pg_catalog.setval('public.users_id_seq', 4, true);


--
-- Name: groups_pkey; Type: CONSTRAINT; Schema: public; Owner: test1
--

ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- Name: property_items_pkey; Type: CONSTRAINT; Schema: public; Owner: test1
--

ALTER TABLE ONLY public.property_items
    ADD CONSTRAINT property_items_pkey PRIMARY KEY (id);


--
-- Name: user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: test1
--

ALTER TABLE ONLY public.user_groups
    ADD CONSTRAINT user_groups_pkey PRIMARY KEY (id);


--
-- Name: user_properties_pkey; Type: CONSTRAINT; Schema: public; Owner: test1
--

ALTER TABLE ONLY public.user_properties
    ADD CONSTRAINT user_properties_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: test1
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: user_properties_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: test1
--

ALTER TABLE ONLY public.user_properties
    ADD CONSTRAINT user_properties_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--
